"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = void 0;
class UserModel {
    constructor() {
        this.roles = [];
    }
    static fromJSON(jsonUserModel) {
        const userModel = new UserModel;
        Object.assign(userModel, jsonUserModel);
        return userModel;
    }
}
exports.UserModel = UserModel;
//# sourceMappingURL=usermodel.js.map