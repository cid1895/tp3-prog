import { Role } from '../enum/role';
export declare class UserModel {
    userId: number;
    name: string;
    email: string;
    roles: Role[];
    constructor();
    static fromJSON(jsonUserModel: UserModel): UserModel;
}
//# sourceMappingURL=usermodel.d.ts.map