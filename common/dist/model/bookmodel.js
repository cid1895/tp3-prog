"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookModel = void 0;
class BookModel {
    static fromJSON(jsonBookModel) {
        const bookModel = new BookModel;
        Object.assign(bookModel, jsonBookModel);
        return bookModel;
    }
}
exports.BookModel = BookModel;
//# sourceMappingURL=bookmodel.js.map