export declare class BookModel {
    bookId: number;
    title: string;
    pageCount?: number;
    static fromJSON(jsonBookModel: BookModel): BookModel;
}
//# sourceMappingURL=bookmodel.d.ts.map