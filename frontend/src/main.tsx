import { BookEditor } from 'component/bookeditor';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<BookEditor />, document.getElementById('coreContainer'));
